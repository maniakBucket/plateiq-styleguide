angular
  .module('app', [
    'ngRoute',
    'ngMaterial',
    'duScroll',
    'hljs',
    'ui.select',
    'ui.bootstrap',
    'ui.scrollpoint',
    'ngSanitize',
    'angular-clipboard',
    'ngAnimate',
    'toastr'
  ]);
