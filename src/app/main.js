var app = angular.module('app');

app
.component('app', {
  templateUrl: 'app/main.html'
});

app
.config(function($routeProvider, hljsServiceProvider, toastrConfig) {
  $routeProvider
  .when('/', {
    templateUrl : 'app/design.html'
  })
  .when('/design', {
    templateUrl : 'app/design.html'
  })
  .when('/components', {
    templateUrl : 'app/components.html'
  })
  .when('/tables', {
    templateUrl : 'app/tables.html'
  })
  .when('/invoices', {
    templateUrl : 'app/invoices.html'
  });

  hljsServiceProvider.setOptions({
    tabReplace: '  '
  });

  angular.extend(toastrConfig, {
    allowHtml: true,
    closeButton: true,
    closeHtml: '<button>&times;</button>',
    extendedTimeOut: 1000,
    positionClass: 'toast-top-center',
    iconClasses: {
      error: 'toast-error',
      info: 'toast-info',
      success: 'toast-success',
      warning: 'toast-warning'
    },
    messageClass: 'toast-message',
    onHidden: null,
    onShown: null,
    onTap: null,
    progressBar: false,
    tapToDismiss: false,
    timeOut: 5000,
    titleClass: 'toast-title',
    toastClass: 'piq-toast toast'
  });
});
//
// app
// .config(function (hljsServiceProvider) {
//   hljsServiceProvider.setOptions({
//     // replace tab with 2 spaces
//     tabReplace: 'adsfasdfsadf'
//   });
// });


app
.controller('ComponentsController', function ($http, $scope, $uibModal, $timeout, $document, $log, toastr, clipboard) {
  var vm = this;

  $scope.totalItems = 250;
  $scope.currentPage = 1;
  $scope.itemsPerPage = 50;
  $scope.searchTerm = "";
  $scope.showPanelContent = true;
  $scope.typeaheadSelected = undefined;

  $scope.showIndividualApprover = true;
  $scope.showAccordionContent1 = true;
  $scope.showAccordionContent2 = false;

  $scope.showFilter = false;
  $scope.applyFilter = false;

  $scope.clearFilters = function () {
    $scope.filterDocumentType = "All Documents";
    $scope.filterState = "Uploaded";
    $scope.filterTimeRange = "Anytime";
    $scope.filterVendor = {};
  };
  $scope.clearFilters();

  $scope.clearSearch = function () {
    $scope.searchTerm = "";
  };

  $scope.clearAutocomplete = function () {
    $scope.autocompleteSelected = "";
  };

  $http.get("uploads.json").then(function(response) {
    $scope.uploads = response.data;
  });
  $http.get("invoices.json").then(function(response) {
    $scope.invoices = response.data;
  });


  $scope.selectedCheckbox = [];
  $scope.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };
  $scope.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };
  $scope.isIndeterminate = function() {
    return ($scope.selectedCheckbox.length !== 0 && $scope.selectedCheckbox.length !== $scope.invoices.length);
  };
  $scope.isChecked = function() {
    return $scope.selectedCheckbox.length === $scope.invoices.length;
  };
  $scope.toggleAll = function() {
    if ($scope.selectedCheckbox.length === $scope.invoices.length) {
      $scope.selectedCheckbox = [];
    } else if ($scope.selectedCheckbox.length === 0 || $scope.selectedCheckbox.length > 0) {
      $scope.selectedCheckbox = $scope.invoices.slice(0);
    }
  };


  $scope.states = [
    'Alabama',
    'Alaska',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'Florida',
    'Georgia',
    'Hawaii',
    'Idaho',
    'Illinouis',
    'Indiana',
    'Iowa',
    'Kansas',
    'Kentucky',
    'Lousiana',
    'Maine',
    'Maryland',
    'Massachusetts',
    'Michigan',
    'Minnesota',
    'Mississipi',
    'Missouri',
    'Montana',
    'Nebraska',
    'Nevada',
    'New Hampshire',
    'New Jersey',
    'New Mexico',
    'New York'
  ];

  $scope.person = {};

  $scope.people = [
    {name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
    {name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
    {name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
    {name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
    {name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
    {name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
    {name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
    {name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
    {name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
    {name: 'Nicolás',   email: 'nicolas@email.com',    age: 43, country: 'Colombia' },
    {name: 'Person with a very very long name',   email: 'nicolas@email.com',    age: 43, country: 'Colombia' }
  ];


  var $ctrl = this;
  $ctrl.animationsEnabled = true;

  $ctrl.openDefaultModal = function (size) {
    var modalInstance = $uibModal.open({
      templateUrl: 'defaultModal.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      windowClass: 'piq-modal',
      size: size,
      resolve: {
        items: function () {
          return
        }
      }
    });
  };

  $ctrl.openConfirmationModal = function (size) {
    var modalInstance = $uibModal.open({
      templateUrl: 'confirmationModal.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      windowClass: 'piq-modal',
      size: size,
      resolve: {
        items: function () {
          return
        }
      }
    });
  };
  $ctrl.openDeleteModal = function (size) {
    var modalInstance = $uibModal.open({
      templateUrl: 'deleteModal.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      windowClass: 'piq-modal',
      size: size,
      resolve: {
        items: function () {
          return
        }
      }
    });
  };
  $ctrl.openActionAreaModal = function (size) {
    var modalInstance = $uibModal.open({
      templateUrl: 'actionAreaModal.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      size: size,
      resolve: {
        items: function () {
          return
        }
      }
    });
  };

  $scope.randomProgress = function() {
    var value = Math.floor(Math.random() * 100 + 1);
    var type;

    if (value < 25) {
      type = 'success';
    } else if (value < 50) {
      type = 'info';
    } else if (value < 75) {
      type = 'warning';
    } else {
      type = 'danger';
    }

    $scope.showWarning = type === 'danger' || type === 'warning';
    $scope.dynamic = value;
    $scope.type = type;
  };
  $scope.randomProgress();


  $scope.copyHex = function (e) {
    var target = e.currentTarget;
    var hex = $('.hex',target).html()
    clipboard.copyText(hex);

    $(target).addClass('copied')
    $timeout(function () {
      $(target).removeClass('copied')
    }, 1000);
  };

  $scope.loading = false;
  $scope.buttonLoad = function (e) {
    $scope.loading = true;
    $timeout(function () {
      $scope.loading = false;
    }, 2000);
  };



  $scope.loginError = false;
  $scope.loginLoading = false;

  $scope.buttonLogin = function (e) {
    $scope.loginLoading = true;
    $scope.loginError = false;
    $timeout(function () {
      $scope.loginLoading = false;
      $scope.loginError = true;
    }, 1000);
  };


  $scope.showToastSuccess = function () {
    toastr.success('Nice! All your changes have been saved successfully!.');
  };
  $scope.showToastError = function () {
    toastr.error('Oops! An error occurred and our team has already been notified. Please try again later.');
  };
  $scope.showToastInfo = function () {
    toastr.info('Please note: You need to do this and that befor everything works.');
  };
  $scope.showToastWarning = function () {
    toastr.warning('Warning: You might lose your changes if you lose your changes.');
  };




  $(document).ready(function() {
      $('.date').datepicker({
          format: 'MM dd, yyyy',
          autoclose: true
      });

      function rgb2hex(rgb) {
        if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        function hex(x) {
          return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
      }

      $('.color-hue').each(function() {
        var rgb = $(this).css('backgroundColor');
        var rgba = "rgba" + rgb.slice(3,-1) + ", 1)";
        var hex = rgb2hex(rgb).toUpperCase();
        $('.rgba', this).prepend(rgba);
        $('.hex', this).prepend(hex);
        $(this).prepend('<h4 class="copy-hex">Copy hex code</h4><h4 class="copied-hex">Copied</h4>');
      })

      $('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
      });

  });

});





// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

angular.module('app').controller('ModalInstanceCtrl', function ($uibModalInstance) {
  var $ctrl = this;

  $ctrl.ok = function () {
    $uibModalInstance.close();
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});




angular.module('app').controller('NavController', function ($scope, $location) {
  console.log($scope, $location);

  $scope.isActive = function (viewLocation) {
    return viewLocation === $location.path();
  };

});
