const gulp = require('gulp');
const sass = require('gulp-sass');
var argv = require('yargs').argv;
var gutil = require('gulp-util');
var awspublish = require('gulp-awspublish');
const concat = require('gulp-concat');

const conf = require('../conf/gulp.conf');

gulp.task('buildStandalone', function(done){
  gulp.src(conf.path.src('scss/standalone/*.scss'))
      .pipe(sass())
      .pipe(gulp.dest(conf.path.dist('/styles')));
  done();
})


gulp.task('buildSrcComponents', function(done){
  gulp.src(conf.path.src('scss/components/*.scss'))
      .pipe(sass())
      .pipe(concat('piq-src-component.css'))
      .pipe(gulp.dest(conf.path.dist('/assets/styles')));
  done();
})

gulp.task('buildAllStyles', function(done){
  gulp.src(conf.path.src('scss/standalone/*.scss'))
      .pipe(sass())
      .pipe(concat('piq-master-component.css'))
      .pipe(gulp.dest(conf.path.dist('/assets/styles')));
  done();
})

gulp.task('buildAllJs', function(done){
  gulp.src(conf.path.src('app/main.js'))
    .pipe(concat('piq-master-component-angular.js'))
    .pipe(gulp.dest(conf.path.dist('/assets/js')));
  done();
})

gulp.task('doDeploy', function(done){
  var bucketName = 'assets.plateiq.com';

  console.log("run do deploy to: " + bucketName);

  var publisher = awspublish.create({
    params : {
      Bucket: bucketName
    }
  });

  // define custom headers
  var headers = {
    'Cache-Control': 'max-age=86400, no-transform, public'
  };

  var indexHeaders = {
    'Cache-Control': 'max-age=60, no-transform, public'
  };

  gulp.src(['dist/assets/styles/piq-master-component.css', 'dist/assets/js/piq-master-component-angular.js'])
    .pipe(awspublish.gzip({ ext: '' }))
    .pipe(publisher.publish(headers))
    .pipe(publisher.cache())
    .pipe(awspublish.reporter());

  done();
})

// function doDeploy() {
//
//   var bucketName = 'dashboard.qubiqle.assets';
//
//   console.log(bucketName);
//
//   if( bucketName ) {
//     var publisher = awspublish.create({
//       params : {
//         Bucket: bucketName
//       }
//     });
//
//     // define custom headers
//     var headers = {
//       'Cache-Control': 'max-age=86400, no-transform, public'
//     };
//
//     var indexHeaders = {
//       'Cache-Control': 'max-age=60, no-transform, public'
//     };
//
//     return gulp.src(['dist/**/*.*'])
//       .pipe(awspublish.gzip({ ext: '' }))
//       .pipe(publisher.publish(headers))
//       .pipe(publisher.cache())
//       .pipe(awspublish.reporter());
//   }
// }

// gulp.task('deploy', gulp.parallel('buildAllStyles', 'buildAllJs'), function(){
//   return doDeploy();
// });

gulp.task('deploy', gulp.series(gulp.parallel('buildAllStyles', 'buildAllJs'), 'doDeploy'));

// gulp.task('deploy', ['buildAllStyles', 'buildAllJs'], function(){
//   doDeploy();
// });
