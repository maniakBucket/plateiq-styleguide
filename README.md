Master Component
===

This is the master component used to prototype new components, it also acts as a reference for the design language.


## Installation

* `npm install`
* `npm run bower -- install` or `bower install`

## Development

`npm run serve` or `./node_modules/.bin/gulp serve`
